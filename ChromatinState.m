%load chromatin data
addpath(genpath('./3rdParty'));
addpath(genpath('./utilities'));

load('./input/S2sm50opt.mat');

%load model
load ./input/model.mat model;
w = model.w;

% integrate
chrom_data=bindata;

nNodes=length(chrom_data(1,:));
nInstances=length(chrom_data(:,1));
nStates = 2;

adj = ones(nNodes)-eye(nNodes);

edgeStruct = UGM_makeEdgeStruct(adj,nStates,1,1000000);
maxState = max(nStates);
nEdges = edgeStruct.nEdges;

nodeMap = zeros(nNodes,maxState,'int32');
for nodeFeat = 1:nNodes
   nodeMap(nodeFeat,2) = nodeFeat;
end
% Make edgeMap
f = max(nodeMap(:));
edgeMap = zeros(nStates,nStates,nEdges,'int32');
for edgeFeat = 1:nEdges
   edgeMap(2,2,edgeFeat) = f+edgeFeat;
end

[nodePot,edgePot] = UGM_MRF_makePotentials(w,nodeMap,edgeMap,edgeStruct);


%% Walk!
chrom_data=bindata;
% nminima=0;
% minima=zeros(1000,nNodes)-1;
% Mcount=0;
% Mpot=0;
% Chrom_minima=zeros(1, length(chrom_data(:,1)));

nNodes=73;
[chrom_data_unique junk countcInd]  =  unique(chrom_data, 'rows') ;
[junk countc]=count_unique(countcInd);countc=countc';


y = chrom_data_unique;
y_final= zeros(size(y));
mapping=1:length(y(:,1));
while ( ~isempty(y) )
    %Try all neighbours (move one step)
    potNei = zeros(length(y(:,1)),nNodes+1);
    parfor i = 1:nNodes
        ynei = y;
        ynei(:,i)= ~y(:,i);
        potNei(:,i) = UGM_Highorder_ConfigurationPotential_batch(ynei,model,nNodes);
    end
    potNei(:,nNodes+1)= UGM_Highorder_ConfigurationPotential_batch(y,model,nNodes);

    [junk I]= max(potNei,[],2);

    for i=1:nNodes
    y(I==i,i)= ~y(I==i,i);
    end
    y_final(mapping(I==nNodes+1),:)=y(I==nNodes+1,:);

    y=y(I~=nNodes+1,:);  
    mapping = mapping(I~=nNodes+1);
    length(y(:,1))
end

    
[minima, junk, countminima]  =  unique(y_final, 'rows') ;
Mcount=zeros(length(minima(:,1)),1);
nminima=length(Mcount);
for i = 1:length(countminima)
    Mcount(countminima(i))=Mcount(countminima(i))+countc(i);
end
Chrom_minima=countminima(countcInd);
Mpot=UGM_Highorder_ConfigurationPotential_batch(minima,model,nNodes);


for i = 1:nminima
    statemeans(i,:)=mean(bindata(Chrom_minima==i,:),1);
end
save('./output/S2sm50opt.chromatinStatePred.raw.mat',...
    'Chrom_minima','minima','Mcount','Mpot','nminima','y_final','statemeans')


