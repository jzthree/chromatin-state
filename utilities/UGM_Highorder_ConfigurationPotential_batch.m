function [pot] = UGM_Highorder_ConfigurationPotential_batch(y,model,nNodes)
% [pot] = UGM_ConfigurationPotential(y,nodePot,edgePot,edgeEnds)



nInstances=length(y(:,1));

logpot =zeros(nInstances,1);
baseind=0;
% Nodes
for n = 1:nNodes
    logpot = logpot + (y(:,n)).*model.w(n+baseind);
end

baseind=baseind+nNodes;
% Edges
for e = 1:length(model.edges2(:,1))
    n1 = model.edges2(e,1);
    n2 = model.edges2(e,2);
    logpot = logpot + (y(:,n1)& y(:,n2)).*model.w(e+baseind);
end
baseind=baseind+length(model.edges2(:,1));

for e = 1:length(model.edges3(:,1))
    n1 = model.edges3(e,1);
    n2 = model.edges3(e,2);
    n3 = model.edges3(e,3);
    logpot = logpot + (y(:,n1)& y(:,n2) & y(:,n3)).*model.w(e+baseind);
end
baseind=baseind+length(model.edges3(:,1));

for e = 1:length(model.edges4(:,1))
    n1 = model.edges4(e,1);
    n2 = model.edges4(e,2);
    n3 = model.edges4(e,3);
    n4 = model.edges4(e,4);
    logpot = logpot + (y(:,n1)& y(:,n2) & y(:,n3) & y(:,n4)).*model.w(e+baseind);
end
baseind=baseind+length(model.edges4(:,1));

for e = 1:length(model.edges5(:,1))
    n1 = model.edges5(e,1);
    n2 = model.edges5(e,2);
    n3 = model.edges5(e,3);
    n4 = model.edges5(e,4);
    n5 = model.edges5(e,5);
    logpot = logpot + (y(:,n1)& y(:,n2) & y(:,n3) & y(:,n4) & y(:,n5)).*model.w(e+baseind);
end
baseind=baseind+length(model.edges5(:,1));

for e = 1:length(model.edges6(:,1))
    n1 = model.edges6(e,1);
    n2 = model.edges6(e,2);
    n3 = model.edges6(e,3);
    n4 = model.edges6(e,4);
    n5 = model.edges6(e,5);
    n6 = model.edges6(e,6);
    logpot = logpot + (y(:,n1)& y(:,n2) & y(:,n3) & y(:,n4) & y(:,n5) & y(:,n6)).*model.w(e+baseind);
end
baseind=baseind+length(model.edges6(:,1));

for e = 1:length(model.edges7(:,1))
    n1 = model.edges7(e,1);
    n2 = model.edges7(e,2);
    n3 = model.edges7(e,3);
    n4 = model.edges7(e,4);
    n5 = model.edges7(e,5);
    n6 = model.edges7(e,6);
    n7 = model.edges7(e,7);
    logpot = logpot + (y(:,n1)& y(:,n2) & y(:,n3) & y(:,n4) & y(:,n5) & y(:,n6) & y(:,n7)).*model.w(e+baseind);
end
pot=exp(logpot);