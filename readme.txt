Here are the scripts for generating chromatin states as described in "Probabilistic Modeling of Chromatin Code Landscape Reveals Functional Diversity of Enhancer-like Chromatin States. Jian Zhou and Olga G. Troyanskaya".

The main scripts are two files, a matlab script ChromtinState.m which generate raw chromatin states, and a R script IntegrateStates.R which iteratively combines mini-states to generate the final 30 states.

Input:  1. binarized chromatin profiles (200bp bin, 50bp stepsize). 2. chromatin model (code for learning chromatin model can be found in http://www.princeton.edu/~jzthree/datasets/PLOS2014/ ). Both inputs are  provided in the input directory.

Output: Bed format genome browser tracks for the chromatin states.